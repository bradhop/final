from flask import redirect, request, url_for, render_template
from flask.views import MethodView
from google.cloud import vision
import unirest
import os

def detect_faces(path):
    client = vision.ImageAnnotatorClient()
    image = vision.types.Image()
    separated = path.split('/')
    imgname, imgext = os.path.splitext(separated[-1])
    response = unirest.get("https://imgur-apiv3.p.rapidapi.com/3/image/" + imgname,
      headers={
        "X-RapidAPI-Host": "imgur-apiv3.p.rapidapi.com",
        "X-RapidAPI-Key": "68ec07b1e9mshea35c77d1c985fdp1b2fe7jsnebdd5e438c7d",
        "Authorization": "Client-ID 091ed3f871b38f4"
      }
    )
    if response.code == 200:
      dims = (response.body.get('data').get('width'), response.body.get('data').get('height'))

      uri = response.body.get('data').get('link')

      image.source.image_uri = uri;
      response = client.face_detection(image=image)
      faces = response.face_annotations
      return faces, dims, uri
    return None, None, None

class Detectfaces(MethodView):
  def get(self):
    return render_template('detectfaces.html')

  def post(self):
    uri = request.form['link']
    result, dims, newuri = detect_faces(uri)

    if result is None:
     return render_template('detectfaces.html')
    else: return render_template('result.html',result = result, uri = newuri, dims = dims)
