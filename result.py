from flask import redirect, request, url_for, render_template
from flask.views import MethodView
from google.cloud import vision

class Result(MethodView):
  def get(self):
    return render_template('result.html', result=result, uri=uri)
