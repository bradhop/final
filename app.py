import flask
from flask.views import MethodView
from index import Index
from detectfaces import Detectfaces
from result import Result

app = flask.Flask(__name__)

app.add_url_rule('/',
                 view_func=Index.as_view('index'),
                 methods=["GET"])

app.add_url_rule('/detectfaces',
                 view_func=Detectfaces.as_view('detectfaces'),
                 methods=["GET","POST"])

app.add_url_rule('/result/',
                 view_func=Result.as_view('result'),
                 methods=["GET"])

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000, debug=True)
