from flask import redirect, render_template
from flask.views import MethodView

class Index(MethodView):
  def get(self):
    return render_template('index.html')
